const activeWin = require('active-win');

let current = {};
setInterval(run, 200);
console.clear();
run();

async function run() {
  console.log(await activeWin())
  const { title, owner: { name }} = await activeWin();
  if(current.title == title && current.owner == name) return;
  current.title = title;
  current.owner = name;
  console.log(format(title, name));
}

function format(title, owner) {
  return `${now()}
    title: ${title}
    owner: ${owner}
  `;
}

function now() {
  const d = new Date();
  const h = (d.getHours() + '').padStart(2, '0');
  const m = (d.getMinutes() + '').padStart(2, '0');
  const s = (d.getSeconds() + '').padStart(2, '0');
  return `${h}:${m}:${s}`;
}
