# What is remoteControl ?

It's two applications, the [server](https://gitlab.com/Noukkis/remotecontrol.server) and the [client](https://gitlab.com/Noukkis/remotecontrol.client).
Those applications work together to create a system for you to remotely control your computer (or whatever device the srever run on) with the client (an Android app)

# Wait, but there are tons of these things aren't they ?
Yeap, surely but this one has a twist : You can fully customize the remote to place buttons that suits actions that fill your needs

# Great! How does it work ?
Follow the [wiki](https://gitlab.com/Noukkis/remotecontrol.server/wikis/home)

# And where do I download the thing ?
You can find the last server version [here](https://gitlab.com/Noukkis/remotecontrol.server/-/jobs/artifacts/master/download?job=deploy)
For the Android APK, you can download it [there](https://gitlab.com/Noukkis/remotecontrol.client/-/jobs/artifacts/master/file/remoteControl.apk?job=deploy)