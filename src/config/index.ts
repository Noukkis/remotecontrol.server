import yaml from 'js-yaml';
import { readFileSync, promises as fs } from 'fs';
import logger from '../logger';

const FILE_PATH = 'config/config.yml';

class Config {

  public port: number;
  public layouts: string[];
  public menuLayout: string;

  public constructor() {
    this.port = 80;
    this.layouts = [];
    this.menuLayout = null;
  }

  public set(newConfig: object): void {
    Object.assign(this, newConfig);
  }

  public async write(): Promise<void> {
    const yml = yaml.safeDump(this);
    try {
      await fs.writeFile(FILE_PATH, yml, 'utf8');
      logger.debug('Config writed');
    } catch(error) {
      logger.error('Cannot write config: %s', error.message);
      throw error;
    }
  }

}

export const config = new Config();

export default {

  init(): void {
    try {
      logger.info('Loading config...');
      const configFile = readFileSync(FILE_PATH, 'utf8');
      const loadedConfig = yaml.safeLoad(configFile);
      config.set(loadedConfig);
      logger.info('Config loaded');
    } catch (error) {
      logger.error('Cannot load config.yml: %s', error.message);
      process.exit(1);
    }
  },

  async reload(): Promise<void> {
    try {
      logger.info('Loading config...');
      const configFile = await fs.readFile(FILE_PATH, 'utf8');
      const loadedConfig = yaml.safeLoad(configFile);
      config.set(loadedConfig);
      logger.info('Config loaded');
    } catch (error) {
      logger.error('Cannot load config.yml: %s', error.message);
    }
  }

};