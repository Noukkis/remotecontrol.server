import { Server } from 'socket.io';
import logger from '../logger';
import listener from './listener';
import sender from './sender';

export default {

  init(io: Server): void {
    listener.init(io);
    sender.init(io);
    logger.info('Socket.IO initialized');
  }
  
};
