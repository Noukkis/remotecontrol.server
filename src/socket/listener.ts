import { Server, Socket } from 'socket.io';
import robot from '../app/robot';
import layoutsManager, { layouts } from '../app/layoutsManager';
import logger from '../logger';
import sender from './sender';
import configManager, { config } from '../config';

export default {

  init(io: Server): void {
    io.on('connection', onConnect);
  }

};

async function onConnect(socket: Socket): Promise<void> {
  logger.info('A devices is connected');
  layoutsManager.watch();

  socket.emit('layouts', layouts);
  socket.emit('menu', config.menuLayout);
  socket.emit('layout', layoutsManager.getLayout());
  
  socket.on('action', (action): void => {
    logger.info('received action');
    try {
      if(!Array.isArray(action)) robot.execute(action);
      else action.forEach((com): void => robot.execute(com));
    } catch(err) {
      logger.error(err);
    }
  });

  socket.on('reload', async (): Promise<void> => {
    await configManager.reload();
    await layoutsManager.load();
    sender.emit('layouts', layouts);
    socket.emit('menu', config.menuLayout);
  });

}
