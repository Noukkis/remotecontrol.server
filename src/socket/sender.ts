import { Server } from 'socket.io';
import logger from '../logger';

let io: Server;

export default {

  init(ioLocal: Server): void {
    io = ioLocal;
  },

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  emit(method: string, ...args: any[]): void {
    if(io) io.emit(method, ...args);
    else logger.error('Trying to broadcast message but no socket found');
  }

};
