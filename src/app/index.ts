import createError from 'http-errors';
import express from 'express';
import { join } from 'path';
import morgan from 'morgan';
import logger from '../logger';

logger.info('Starting app...');

const app = express();

// view engine setup
app.set('views', join(__dirname, 'views'));
app.set('view engine', 'pug');

// global setup
app.use(morgan('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Redirect to gui
app.get('/', (req, res): void => {
  res.end('server up');
});

// catch 404 and forward to error handler
app.use((req, res, next): void => {
  next(createError(404));
});

// error handler
app.use((err, req, res, _next): void => {
  // render the error page
  res.status(err.status || 500);
  res.type('html');
  res.render('error');
});

export default app;