export interface Action {
  type: ActionType;
  value: ActionValue;
}

export enum ActionType {
  Keyboard = 'keyboard',
  Mouse = 'mouse',
  Click = 'click',
  Sleep = 'sleep',
}

export interface ActionValue {
  // Keyboard
  key?: string;
  type?: KeyboardActionType;
  down?: string;
  modifier?: string | string[];
  // Mouse
  x?: number;
  y?: number;
  // Click
  button?: ButtonType;
  // sleep
  time?: number;
}

export enum ButtonType {
  Left = 'left',
  Right = 'right',
  Middle = 'middle'
}

export enum KeyboardActionType {
  Tap = 'tap',
  Toggle = 'toggle',
  Type = 'type'
}