import robot from '@morajabi/robotjs';
import sleep from 'system-sleep';
import logger from '../../logger';
import { 
  Action,
  ActionType,
  KeyboardActionType,
  ActionValue
} from './Action';

let STRING_CPM = 2000
robot.setMouseDelay(0);
robot.setKeyboardDelay(0);

// monkey patching robotjs modifier bug
((): void => {
  const keyTap = robot.keyTap;
  robot.keyTap = (key: string, modifier?: string | string[]): void => {
    if(!modifier) return keyTap(key, modifier);
    if(typeof modifier === 'string') modifier = [modifier];
    modifier.forEach((mod): void => robot.keyToggle(mod, 'down'));
    keyTap(key);
    modifier.forEach((mod): void => robot.keyToggle(mod, 'up'));
  };
})();

export default {

  execute(command: Action): void {
    logger.debug(JSON.stringify(command));
    const { Keyboard, Mouse, Click, Sleep } = ActionType;
    switch(command.type) {
    case Keyboard: execKeyboard(command.value);
      break;
    case Mouse: execMouse(command.value);
      break;
    case Click: execClick(command.value);
      break;
    case Sleep: sleep(command.value.time);
      break;
    }
  }

};

function execKeyboard({ key, type, down, modifier }: ActionValue): void {
  const { Tap, Toggle, Type } = KeyboardActionType;
  if(key.length > 1) key = key.toLowerCase();
  switch(type) {
  case Tap: robot.keyTap(key, modifier || []);
    break;
  case Toggle: robot.keyToggle(key, down, modifier || []);
    break;
  case Type: robot.typeStringDelayed(key, STRING_CPM);
    break;
  default: robot.keyTap(key, modifier || []);
  }
}

function execMouse({ x, y }: ActionValue): void {
  robot.moveMouse(x, y);
}

function execClick({ button }: ActionValue): void {
  robot.mouseClick(button);
}
