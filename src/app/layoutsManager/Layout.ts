import { Action } from '../robot/Action';

export interface Layout {
  matcher?: Matcher;
  onEnter?: Action[];
  components: Component[];
}

interface Matcher {
  title?: string;
  owner?: string;
}

interface Component {
  type: ComponentType;
  icon?: string;
  text?: string;
  size?: number;
  actions: Action[];
}

enum ComponentType {
  Button = 'Button',
  ArrowKeys = 'ArrowKeys',
  Mouse = 'Mouse',
  KeyboardButton = 'KeyboardButton'
}