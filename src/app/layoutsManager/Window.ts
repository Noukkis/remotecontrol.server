export interface Window { 
  title: string;
  owner: { 
    name: string;
  };
}