import yaml from 'js-yaml';
import { promises as fs } from 'fs';
import { join } from 'path';
import activeWin from 'active-win';
import { config } from '../../config';
import { Layout } from './Layout';
import logger from '../../logger';
import robot from '../robot';
import sender from '../../socket/sender';
import { Window } from './Window';


const LAYOUTS_PATH = join(__dirname, '../../../config/layouts');
const SLEEP_TIME = 500;

let isWatching = false;
let currentLayout = null;
export let layouts: Layout[] = [];

export default {

  async load(): Promise<void> {
    logger.info('Loading layouts');
    const promises = config.layouts.map(getLayout);
    layouts = await Promise.all(promises);
    logger.info('Layouts loaded');
    logger.debug(JSON.stringify(layouts));
    update();
  },

  watch(): void {
    if(isWatching) return;
    setInterval(update, SLEEP_TIME);
    isWatching = true;
  },

  getLayout(): number {
    return currentLayout;
  }

};

async function update(): Promise<void> {
  let win: Window = await activeWin();
  if(!win) win = { title: '', owner: { name: ''} };
  const layoutIndex = layouts.findIndex((layout: Layout): boolean => {
    if(!layout.matcher) return false;
    const { owner, title } = layout.matcher;
    return (!title || new RegExp(title).test(win.title))
      && (!owner || new RegExp(owner).test(win.owner.name));
  });
  if(layoutIndex == currentLayout) return;
  currentLayout = layoutIndex;
  logger.info('Layout update: ' + layoutIndex);
  if(layoutIndex >= 0) sender.emit('layout', layoutIndex);
  const layout = layouts[layoutIndex];
  if(layout.onEnter) layout.onEnter.forEach((action): void => robot.execute(action));
}

async function getLayout(name: string): Promise<Layout> {
  const file = join(LAYOUTS_PATH, `${name}.yml`);
  const content = await fs.readFile(file, 'utf8');
  const layout = yaml.safeLoad(content);
  return layout;
}
