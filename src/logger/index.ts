import { mkdirSync, existsSync } from 'fs';
import { Logger, LoggerOptions, createLogger, transports, format } from 'winston';

const { Console, File } = transports;
const { combine, timestamp, printf, splat } = format;

if(!existsSync('log')) {
  mkdirSync('log');
}

const logFormat = printf((info): string => {
  return `[${info.timestamp}] ${info.level.toUpperCase()}: ${info.message}`;
});

const options: LoggerOptions = {
  level: process.env.NODE_ENV == 'development' ? 'debug' : 'info',
  transports: [
    new Console(),
    new File({
      filename: 'log/combined.log',
      maxsize: 5242880,
      maxFiles: 5,
      tailable: true
    })
  ],
  format: combine(
    timestamp({format: 'YYYY-MM-DD HH:mm:ss'}),
    splat(),
    logFormat
  )
};

const logger: Logger = createLogger(options);
export default logger;
