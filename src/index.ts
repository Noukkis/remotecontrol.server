/**
 * Module dependencies.
 */
import app from './app';
import logger from './logger';
import socket from './socket';
import { Server } from 'http';
import SocketIO from 'socket.io';
import configManager, { config } from './config';
import layoutsManager from './app/layoutsManager';

configManager.init();

layoutsManager.load().catch((error: Error): void => {
  logger.error('Cannot load layouts: %s', error.message);
  process.exit(1);
});

const server = new Server(app);

/**
 * Get port from environment and store in Express.
 */

const port = config.port;
app.set('port', port);

/**
 * Configure socket.io
 */
const io = SocketIO(server);
socket.init(io);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port);
server.on('error', onError);
server.on('listening', onListening);

/**
 * Event listener for HTTP server "error" event.
 */

function onError(error): void {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string'
    ? 'Pipe ' + port
    : 'Port ' + port;

  // handle specific listen errors with friendly messages
  switch (error.code) {
  case 'EACCES':
    logger.error(bind + ' requires elevated privileges');
    process.exit(1);
    break;
  case 'EADDRINUSE':
    logger.error(bind + ' is already in use');
    process.exit(1);
    break;
  default:
    throw error;
  }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening(): void {
  logger.info('Listening on http://localhost:' + port);
}
